import { Component, ElementRef, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  
  constructor(private elementRef: ElementRef) {
    
  }

  ngAfterViewInit() {
    // Inicializa el botón de inicio de sesión de Facebook
    FB.XFBML.parse(this.elementRef.nativeElement);
  }

  loginWithFacebook() {
    // Realiza la autenticación con Facebook
    debugger
    FB.login((response: any) => {
      debugger
      if (response.authResponse) {
        // El usuario ha iniciado sesión correctamente
        console.log('Token de acceso:', response.authResponse.accessToken);

        
      } else {
        // El usuario canceló el inicio de sesión o hubo un error
        console.error('Inicio de sesión con Facebook cancelado.');
      }
    }, { scope: 'email,public_profile' });
  }

  me() {
    FB.api('/me', (response: any) => {
      debugger
      console.log('Successful login for: ' + response.name);
    });
  }
}
